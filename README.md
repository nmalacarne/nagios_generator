# Nagios Object Definition Generator

Generate object definition files for Nagios from CSV files.

`python3 config_generator --help` for options. 

### Supported input file formats:

- CSV

Object definition types will be inferred based on the default fields found [here](https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/3/en/objectdefinitions.html).
