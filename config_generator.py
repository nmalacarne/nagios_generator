import argparse
import csv

from enum import Enum


class DefType(Enum):
    HOST = 'host'
    HOSTGROUP = 'hostgroup'
    SERVICE = 'service'
    SERVICEGROUP = 'servicegroup'
    CONTACT = 'contact'
    CONTACTGROUP = 'contactgroup'
    TIMEPERIOD = 'timeperiod'
    COMMAND = 'command'
    HOSTDEPENDENCY = 'hostdependency'
    HOSTESCALATION = 'hostescalation'
    SERVICEDEPENDENCY = 'servicedependency'
    SERVICEESCALATION = 'serviceescalation'
    UNKNOWN = 'unknown'


REQUIRED_FIELDS = {
    DefType.HOST: {
        'host_name',
        'address',
        'max_check_attempts',
        'check_period',
        'contacts',
        'contact_groups',
        'notification_interval',
        'notification_period',
    },
    DefType.HOSTGROUP: {
        'hostgroup_name',
        'alias',
    },
    DefType.SERVICE: {
        'host_name',
        'service_description',
        'check_command',
        'max_check_attempts',
        'check_period',
        'contacts',
        'contact_groups',
        'notification_interval',
        'notification_period',
    },
    DefType.SERVICEGROUP: {
        'servicegroup_name',
        'alias',
    },
    DefType.CONTACT: {
        'contact_name',
        'host_notification_enabled',
        'service_notification_enabled',
        'host_notification_period',
        'service_notification_period',
        'host_notification_options',
        'service_notification_options',
        'host_notification_commands',
        'service_notification_commands',
    },
    DefType.CONTACTGROUP: {
        'contactgroup_name',
        'alias',
    },
    DefType.TIMEPERIOD: {
        'timeperiod_name',
        'alias',
    },
    DefType.COMMAND: {
        'command_name',
        'command_line',
    },
    DefType.SERVICEDEPENDENCY: {
        'dependent_host_name',
        'dependent_service_description',
        'host_name',
        'service_description',
    },
    DefType.HOSTDEPENDENCY: {
        'dependent_host_name',
        'host_name',
    },
    DefType.HOSTESCALATION: {
        'host_name',
        'service_description',
        'first_notification',
        'last_notification',
        'notification_interval',
        'contacts',
        'contact_groups',
    },
    DefType.SERVICEESCALATION: {
        'host_name',
        'service_description',
        'first_notification',
        'last_notification',
        'notification_interval',
    },
}


def read_line(file_path: str) -> dict:
    with open(file_path, 'r') as csvfile:
        reader = csv.DictReader(csvfile)

        for line in reader:
            yield line


def get_def_type(data: dict) -> DefType:
    keys = set({k.lower(): v for k, v in data.items()})

    for k, v in REQUIRED_FIELDS.items():
        if v <= keys:
            return k
    else:
        return DefType.UNKNOWN


def dict_to_def(data: dict) -> str:
    t = f"define {get_def_type(data).value} {{\n"

    for k, v in data.items():
        t += f"\t{k.lower().ljust(20)}\t{v}\n"

    t += "}\n"

    return t


def generate_definitions(input_path: str, output_path: str):
    with open(output_path, 'w+') as output_file:
        for line in read_line(input_path):
            output_file.write(dict_to_def(line))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Parse files and generate Nagios object definitions."
    )

    parser.add_argument('input', help='Input file path (CSV).')
    parser.add_argument(
        '--output',
        '-o',
        help='Write object definitions to this file (defaults to STDOUT).'
    )

    args = parser.parse_args()

    if args.output is None:
        for line in read_line(args.input):
            print(dict_to_def(line))
    else:
        generate_definitions(args.input, args.output)
