import unittest

from config_generator import (
    get_def_type,
    dict_to_def,
    DefType
)


class TestFuncs(unittest.TestCase):
    def setUp(self):
        self.host = {
            'host_name': 'test_host',
            'address': '127.0.0.1',
            'max_check_attempts': '3',
            'check_period': '24x7',
            'contacts': 'admin',
            'contact_groups': 'admin',
            'notification_interval': '5',
            'notification_period': '24x7',
        }

        self.hostgroup = {
            'hostgroup_name': 'test_hostgroup',
            'alias': 'test_alias',
        }

        self.service = {
            'host_name': 'test_service',
            'service_description': 'A Test Service.',
            'check_command': 'ping',
            'max_check_attempts': '3',
            'check_period': '24x7',
            'contacts': 'admin',
            'contact_groups': 'admin',
            'notification_interval': '5',
            'notification_period': '24x7',
        }

        self.servicegroup = {
            'servicegroup_name': 'test_services',
            'alias': 'Test Services',
        }

        self.contact = {
            'contact_name': 'test_contact',
            'host_notification_enabled': '0',
            'service_notification_enabled': '0',
            'host_notification_period': '24x7',
            'service_notification_period': '24x7',
            'host_notification_options': 'd,u',
            'service_notification_options': 'd,u',
            'host_notification_commands': 'test_notify',
            'service_notification_commands': 'test_notify',
        }

        self.contactgroup = {
            'contactgroup_name': 'test_contactgroup',
            'alias': 'Test Contact Group',
        }

        self.timeperiod = {
            'timeperiod_name': 'test_timeperiod',
            'alias': 'Test Timeperiod'
        }

        self.command = {
            'command_name': 'test_command',
            'command_line': 'ping',
        }

        self.hostdependency = {
            'dependent_host_name': 'test_dependenthost',
            'host_name': 'test_host',
        }

        self.servicedependency = {
            'dependent_host_name': 'test_dependenthost',
            'dependent_service_description': 'test_dependenthost',
            'host_name': 'test_host',
            'service_description': 'test_service',
        }

        self.hostescalation = {
            'host_name': 'test_host',
            'service_description': 'test_description',
            'first_notification': '1',
            'last_notification': '2',
            'notification_interval': '1',
            'contacts': 'test_contact',
            'contact_groups': 'test_group',
        }

        self.serviceescalation = {
            'host_name': 'test_host',
            'service_description': 'test_description',
            'first_notification': '1',
            'last_notification': '2',
            'notification_interval': '1',
        }

    def test_get_def_type(self):
        self.assertEqual(DefType.HOST, get_def_type(self.host))
        self.assertEqual(DefType.HOSTGROUP, get_def_type(self.hostgroup))
        self.assertEqual(DefType.SERVICE, get_def_type(self.service))
        self.assertEqual(DefType.SERVICEGROUP, get_def_type(self.servicegroup))
        self.assertEqual(DefType.CONTACT, get_def_type(self.contact))
        self.assertEqual(DefType.CONTACTGROUP, get_def_type(self.contactgroup))
        self.assertEqual(DefType.TIMEPERIOD, get_def_type(self.timeperiod))
        self.assertEqual(DefType.COMMAND, get_def_type(self.command))
        self.assertEqual(
            DefType.HOSTDEPENDENCY,
            get_def_type(self.hostdependency)
        )
        self.assertEqual(
            DefType.SERVICEDEPENDENCY,
            get_def_type(self.servicedependency)
        )
        self.assertEqual(
            DefType.HOSTESCALATION,
            get_def_type(self.hostescalation)
        )
        self.assertEqual(
            DefType.SERVICEESCALATION,
            get_def_type(self.serviceescalation)
        )

    def test_dict_to_def(self):
        expected_str = (
            "define host {\n"
            f"\thost_name{' ' * 11}\ttest_host\n"
            f"\taddress{' ' * 13}\t127.0.0.1\n"
            f"\tmax_check_attempts{' ' * 2}\t3\n"
            f"\tcheck_period{' ' * 8}\t24x7\n"
            f"\tcontacts{' ' * 12}\tadmin\n"
            f"\tcontact_groups{' ' * 6}\tadmin\n"
            f"\tnotification_interval\t5\n"
            f"\tnotification_period \t24x7\n"
            "}\n"
        )

        self.assertEqual(expected_str, dict_to_def(self.host))


if __name__ == '__main__':
    unittest.main()
